%
% The second chapter in face recognition
%
\chapter{Implementation of The Hybrid Model}
\hspace{50pt}Implementation details of the hybrid model outlined in the design chapter are described here. The implementation is also benchmarked
 for proof of concept (details of which are provided in later chapters). The design of the implementation was done with the intent to develop a tool to foster further research in this area.
%
%
\section{Modular Design details}
%
\begin{figure}[!h]
\centering
\includegraphics[scale=0.40]{fr_flowchartmodules}
\caption{Stages in the model}
\label{fr_flowchartmodules}
\end{figure}
%
\hspace{50pt}A modular design has been followed in implementing the proposed model, with modules as outlined above. The input to the first module is the image or the video input on a frame-by-frame basis, from which faces are to be recognised. The first module feeds its output - landmarks in the faces in the scene to the second module that computes a feature vector from the same. The feature vectors are fed as input to the neural network acting as a classifier. The output of the classifier is a class number which is interpreted by the final module to identify the person in the image or frame and complete the process.

\hspace{50pt}Each module has been given individual identity with clear interface definitions to allow for easy modification of any single module independently. The working of a module can be changed or the entire module replaced with another as long as the interface is satisfied. This ease of
 modification has been provided to enable the implementation to be used for further research. Various design choices are available for the internal working of each module. One such implementation with the choices made for each module is described next.
%
%
%
%
\section{Preprocessing Stage}
\hspace{50pt}This stage interfaces the input stream with the program. A video stream input is considered on a frame-by-frame basis. Each individual frame is processed to obtain faces that are fed as input to the next stage. To achieve this, first each frame is converted to monochrome and histogram equlaisation is carried out. The equalised frame is then operated on by OpenCV's face detection function that utilises Haar Cascades. The function used is
\texttt{detectMultiScale} and the Haar Cascade file used is \texttt{frontface\_alt.xml}. This function returns the faces recognised in the image as an array of rectangles. Each element in this array is fed as input to the next stage.
%
%
\section{Finding Landmarks}
\hspace{50pt}This stage takes a face identified in the image as input and uses a landmarking tool to extract landmarks from it. The tool used here is Clandmark \cite{fr_clandmark-main}. The tool requires the conversion of the image into a \texttt{CImg} array prior to processing. A function \texttt{cvImgToCImg} is used for this. A registered instance of the landmarker is used in this stage. Landmarks are calculated using the \texttt{detect\_optimized} method in the landmarker instance. A method \texttt{getLandmarks} returns the landmarks as \(x,y\) pairs in a linear array. The length of this array is given by the \texttt{getLandmarksCount} method. The tool used has two modes of operation, detecting different number of landmark points. The denser mode is utilised here and results in \textbf{75}
 co-ordinate pairs. The various landmark points selected by the tool on a face is shown in fig \ref{fr_face_model}. The array of co-ordinate pairs are given as input to the next stage.

\begin{figure}[h]
\centering
\includegraphics[scale=0.35]{face_model}
\caption{Landmarks identified on a face by Clandmark in dense mode}
\label{fr_face_model}
\end{figure}

%
%

\section{Generating the Feature Vector}
\hspace{50pt}The feature vector generated in this implementation has a dimensionality of \textbf{21}. From the 75 points on the given face, various distances between pairs of points are computed to obtain the components of the vector. This computation is carried out with the intent that the resulting 21 point vector characterises the face sufficiently. The various calculations performed are illustrated in figures below with the thick lines showing the measurements made on the face between landmark pairs.

\begin{figure}[!h]
\centering
\includegraphics[scale=0.45]{face_chin}
\caption{Distance between corresponding landmark points on either halves of the face computed to provide 8 values that characterise the shape of the face in general}
\label{fr_face_chin}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.45]{face_eyebrows}
\caption{Distances calculated to abstract width and shape of eyebrows}
\label{fr_face_eyebrows}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.45]{face_mouth}
\caption{Width of mouth and relative positions of eyes and mouth determined}
\label{fr_face_mouth}
\end{figure}

\begin{figure}[!h]
\centering
\includegraphics[scale=0.45]{face_nose}
\caption{Distance between endpoints of the eyebrows and the nose calculated to establish relative positions}
\label{fr_face_nose}
\end{figure}

\hspace{50pt}The various distance calculations are made to abstract the shape of the face for easier computation. Choice of distances affects the accuracy of the system. Selection of distances that do not characterise a face well enough would lead to system that will find no difference between two very different faces. All parts of the feature vector taken together describe the layout of the face,for eg: in fig \ref{fr_face_mouth} the relative distances between the eyes and mouth alone are established. Applying the heuristic that the left eye is to the left of the right eye and that the eyes are within the face boundary, the width of the face as given in fig \ref{fr_face_chin} can be used to constrain the position of the eyes. This can be combined with the lengths of nose (fig \ref{fr_face_nose}) and the eyebrows (fig \ref{fr_face_eyebrows}) to approximate the structure of the face.\\
 The feature vector obtained is fed as input to the neural network classifier.
%
%
%
%
%
\clearpage
\section{Neural Network Operation}
\hspace{50pt}The neural network employed in this implementation is a Kohonen Self Organising Map (KSOM). KSOMs are essentially vector classifiers that accept an input vector and respond by identifying the class to which it belongs. The basic variant of KSOM was used for its simple design and ease of implementation. KSOM training follows the Unsupervised learning paradigm wherein the total number of output classes and dimension of the input are specified. Additional parameters such as initial learning rate, learning rate decay rate, initial neighbourhood radius, and neighbourhood radius shrink rate are also specified. Inputs are then fed and the network is allowed to classify the vectors as it sees fit. The results of classification are inferred by the feeding training vectors as input to know which class each vector has been classified into. If required, modifications can be made using learning vector quantisation (this is not done in this implementation).

\hspace{50pt}The KSOM is initialised (once, prior to the first use) using the \texttt{setup\_ksom} \texttt{\_variables} and \texttt{load\_weight\_matrix} methods. The former sets up the number of output nodes, the number of vectors used for training and the dimension of input vectors. The latter loads the weights associated with various connections from a file to finalise connections.
The feature vector calculated is fed to the KSOM by the \texttt{perform\_KSOM\_operations} function call. This single call results in the name and deviation values.

\hspace{50pt}The \texttt{perform\_KSOM\_operations} function first calls the \texttt{find\_winner} met\-hod of the KSOM passing the feature vector as argument. This returns the number of the class containing the template that best matches the given input. The classification itself is many to one and thus further interpretation of the class result obtained is carried out in the next phase.
%
%
\section{Interpreting Results from Neural Network}
\hspace{50pt}As aforementioned, mapping of vectors (and thus identities) into output classes is many to one. Thus, it is necessary to keep track of the various identities in the different classes and interpret the best match class number. The details of classification are stored in a YAML file that is loaded once at startup and stored in an associative map where each class number has a corresponding vector of identities in it. Also, the name and feature vector corresponding to each template are also loaded from files. These load operations are carried out as part of the \texttt{init\_KSOM\_from\_main} function call.

\hspace{50pt}When the class number is obtained, a linear search is carried out in the vector corresponding to it. In case there are no templates in the class, the classification ignored and no further action is taken. Otherwise, the list is traversed and the \emph{deviation} between each template and the given input is calculated. The template with least deviation is selected as the winner and the corresponding name and deviation are passed as results from the \texttt{perform\_ksom\_operations} function.

\hspace{50pt}The identification result is compared against a threshold to detect spurious classifications. Misclassifications result in large deviation values. Based on benchmark scores, a threshold value can be established. If the deviation for current input lies below this threshold, it is accepted as valid identification. Otherwise it is deemed mis-identification and is ignored. Statistics regarding identification, both correct and incorrect can be collected for further study and enhancement of the installation. This is not done here. A sample image showing what an implementation of the system would look like is procided in fig.\ref{fr_VKH_detect}. Here, the person is identified by a bounding rectangle that contains the stored name (in this case, VKH) and the deviation from best match template.

\begin{figure}[!h]
\centering
\includegraphics[scale=1.0]{fr_VKH_detect}
\caption{Illustration of operation of face recognition.}
\label{fr_VKH_detect}
\end{figure}
%
%
\section{Factors affecting performance}
\hspace{50pt}The performance of each module in the system can have varying effects on the overall system performance. A discussion on this follows.
\subsection{Preprocessing Stage performance}
\hspace{50pt}Accurate detection of faces is crucial to performance of the system. Spurious detection leads to wastage of processing cycles as the system spends time trying to detect landmarks on pixels that do not represent a face. On the other hand, if faces are not detected, the purpose of the system is defeated. It is preferred to have a system that can detect faces from a wide range of angles and perspectives. Systems that work despite occlusion is useful only if successive stages can handle occlusion too.

\hspace{50pt}This implementation utilises Haar Cascades for face detection. Various cascades can be used for detection of various objects. Here, the standard \texttt{frontface\_alt.xml} cascades are used, capable of detecting faces from a range of angles. Other means of detecting faces such as histogram matching or model based techniques can also be employed provided their performance is deemed better for the particular application.
%
%
\subsection{Landmarking Stage}
\hspace{50pt}Performance of this stage is determined by the landmarking tool alone. Tools used should be deterministic - given the same input, the positions of landmarks detected should always be the same. Also, the same set of landmarks should be determined for all inputs (homogenity). Tools that give more inputs lead to higher precision in further stages of operation but at the cost of increased processing time. Balance must be struck between number of landmarks and time taken to attain required performance constraints. The tool used, \textbf{Clandmark} meets the requirements of determinism and homogenity. It generates \textbf{75} landmarks in about 1 $\mu$s (deatils of benchmarking platform given in subsequent chapters).
%
%
\subsection{Feature Vector Generation Stage}
\hspace{50pt}The feature vector selected must be one that can characterise a face well enough to distinguish it from a similar face. A vector that is too general will fail to aid in the identification process. On the other hand, selection of the vector must also consider economy of computation. Selecting large dimensionality and utilising complex calculations will incur large processing costs. Further, successive stages are also affected by increased dimensionality of vectors. This implementation uses a vector of \textbf{21} dimension with components being distances between various landmark points that are inexpensive to compute.

\subsection{Neural Network Stage}
\hspace{50pt}The operation of this stage is simply a classification of the input vector. This is achieved by comparing each output class vector to the input vector and finding the class that is 'closest' to the given input. It is possible to carry out the comparison operation in a parallel fashion to improve performance. Further, since the nueral network is itself stateless with respect to classification operation (i.e no change in configuration of the network itself transpires), it is possible to carry out the classification of several input vectors simultaneously (\emph{data parallelism}).

\hspace{50pt}This implementation uses a KSOM with a single thread of execution. This lack of parallelism is justified by the negligible improvements in performance due to parallelism when number of output classes are only a few hundred in number. Larger number of output classes are seldom used by small to medium scale installations.
%
%
\subsection{Results Interpretation Stage}
\hspace{50pt}In this implementation, this stage requires a linear search in the list of all templates in the identified class. Ther performance is thus bounded by the maximum length of such a vector,i.e the maximum number of templates in any given class. This can be changed by modifying the various parameters of the KSOM during training. The presence of a large number of templates in any class increases the maximum bound on search time. On the other hand, restricting the number of templates in any class to too few reqiures a large number of output classes which affects the performance of the KSOM.