\contentsline {chapter}{\numberline {1}Introduction}{1}
\contentsline {chapter}{\numberline {2}Requirements Specification}{3}
\contentsline {section}{\numberline {2.1}Introduction}{3}
\contentsline {subsection}{\numberline {2.1.1}Purpose}{3}
\contentsline {subsection}{\numberline {2.1.2}Document Conventions}{3}
\contentsline {subsection}{\numberline {2.1.3}Intended Audience and Reading Suggestions}{3}
\contentsline {subsection}{\numberline {2.1.4}Product Scope}{4}
\contentsline {subsection}{\numberline {2.1.5}References}{4}
\contentsline {section}{\numberline {2.2}Overall Description}{5}
\contentsline {subsection}{\numberline {2.2.1}Product Perspective}{5}
\contentsline {subsection}{\numberline {2.2.2}Product Functions}{5}
\contentsline {subsection}{\numberline {2.2.3}User Characteristics}{6}
\contentsline {subsection}{\numberline {2.2.4}Operating Environment}{7}
\contentsline {subsection}{\numberline {2.2.5}Design and Implementation Constraints}{7}
\contentsline {subsection}{\numberline {2.2.6}Assumptions and Dependencies}{7}
\contentsline {section}{\numberline {2.3}External Interface Requirements}{8}
\contentsline {subsection}{\numberline {2.3.1}User Interfaces}{8}
\contentsline {subsection}{\numberline {2.3.2}Software Interfaces}{8}
\contentsline {subsection}{\numberline {2.3.3}Communications Interfaces}{8}
\contentsline {section}{\numberline {2.4}System Features}{9}
\contentsline {subsection}{\numberline {2.4.1}Recognition Module}{9}
\contentsline {subsubsection}{Description and Priority}{9}
\contentsline {subsubsection}{Stimulus/Response Sequences}{9}
\contentsline {subsubsection}{Functional Requirements}{9}
\contentsline {subsection}{\numberline {2.4.2}Tracking Module }{9}
\contentsline {subsubsection}{Description and priority}{9}
\contentsline {subsubsection}{Stimulus/Response sequences}{10}
\contentsline {subsubsection}{Functional Requirements}{10}
\contentsline {section}{\numberline {2.5}Other Non-functional Requirements}{10}
\contentsline {subsection}{\numberline {2.5.1}Performance Requirements}{10}
\contentsline {subsection}{\numberline {2.5.2}Safety Requirements}{10}
\contentsline {subsection}{\numberline {2.5.3}Security Requirements}{10}
\contentsline {subsection}{\numberline {2.5.4}Software Quality Attributes}{11}
\contentsline {subsection}{\numberline {2.5.5}Business Rules}{11}
\contentsline {chapter}{\numberline {3}Design of Solution}{12}
\contentsline {section}{\numberline {3.1}Introduction}{12}
\contentsline {section}{\numberline {3.2}Video Analysis}{13}
\contentsline {subsection}{\numberline {3.2.1}Introduction}{13}
\contentsline {subsubsection}{Motion Vector}{13}
\contentsline {subsection}{\numberline {3.2.2}Analysis Procedure}{13}
\contentsline {subsubsection}{Extracting Spatial Information}{13}
\contentsline {subsubsection}{Updating Tracked Objects}{15}
\contentsline {section}{\numberline {3.3}Neural Networks for Motion Analysis}{17}
\contentsline {subsection}{\numberline {3.3.1}Introduction}{17}
\contentsline {subsection}{\numberline {3.3.2}Creating the Neural Network and Forming the Training Set}{17}
\contentsline {subsubsection}{Creating The Training Set}{17}
\contentsline {subsubsection}{Creating Neural Networks}{18}
\contentsline {subsection}{\numberline {3.3.3}Training The Neural Network}{19}
\contentsline {subsubsection}{Parameters Involved In The Training Phase}{19}
\contentsline {subsection}{\numberline {3.3.4}Testing The Neural Network}{19}
\contentsline {section}{\numberline {3.4}Design for Face Recognition}{19}
\contentsline {chapter}{\numberline {4}Implementation of Behaviour Analysis}{21}
\contentsline {section}{\numberline {4.1}Video Analysis}{21}
\contentsline {subsection}{\numberline {4.1.1}Background Subtraction}{21}
\contentsline {subsection}{\numberline {4.1.2}Noise Filtering}{22}
\contentsline {subsection}{\numberline {4.1.3}Blob Detection and Extraction}{22}
\contentsline {subsection}{\numberline {4.1.4}Tracking Algorithm}{22}
\contentsline {section}{\numberline {4.2}Implementing Neural Network}{23}
\contentsline {subsection}{\numberline {4.2.1}Neural Network Creation}{23}
\contentsline {subsubsection}{Platforms And Frameworks Used for Neural Network Creation}{23}
\contentsline {subsubsection}{Creating Training Set}{23}
\contentsline {subsubsection}{Creating Neural Networks}{23}
\contentsline {subsubsection}{Training Neural Network}{24}
\contentsline {subsubsection}{Tesing Neural Network}{24}
\contentsline {chapter}{\numberline {5}Implementation of The Hybrid Model}{25}
\contentsline {section}{\numberline {5.1}Modular Design details}{25}
\contentsline {section}{\numberline {5.2}Preprocessing Stage}{26}
\contentsline {section}{\numberline {5.3}Finding Landmarks}{26}
\contentsline {section}{\numberline {5.4}Generating the Feature Vector}{27}
\contentsline {section}{\numberline {5.5}Neural Network Operation}{30}
\contentsline {section}{\numberline {5.6}Interpreting Results from Neural Network}{30}
\contentsline {section}{\numberline {5.7}Factors affecting performance}{32}
\contentsline {subsection}{\numberline {5.7.1}Preprocessing Stage performance}{32}
\contentsline {subsection}{\numberline {5.7.2}Landmarking Stage}{32}
\contentsline {subsection}{\numberline {5.7.3}Feature Vector Generation Stage}{33}
\contentsline {subsection}{\numberline {5.7.4}Neural Network Stage}{33}
\contentsline {subsection}{\numberline {5.7.5}Results Interpretation Stage}{33}
\contentsline {chapter}{\numberline {6}Graphical User Interface Details}{34}
\contentsline {section}{\numberline {6.1}GUI for Behaviour Analysis}{34}
\contentsline {subsection}{\numberline {6.1.1}GUI for Training}{34}
\contentsline {subsection}{\numberline {6.1.2}GUI for Threat Detection}{35}
\contentsline {section}{\numberline {6.2}GUI for Face Recognition Neural Network Training}{35}
\contentsline {subsection}{\numberline {6.2.1}Layout}{38}
\contentsline {subsubsection}{File Details Fields}{38}
\contentsline {subsubsection}{Neural Network Configuration}{38}
\contentsline {subsubsection}{Buttons Panel}{39}
\contentsline {subsection}{\numberline {6.2.2}Executable for Landmarking}{40}
\contentsline {subsection}{\numberline {6.2.3}Executable for Training KSOM}{40}
\contentsline {chapter}{\numberline {7}Testing}{41}
\contentsline {section}{\numberline {7.1}Testing of Video Analysis and Neural Network Modules}{41}
\contentsline {subsection}{\numberline {7.1.1}Testing and Benchmarking of Background Subtraction}{41}
\contentsline {subsection}{\numberline {7.1.2}Testing Multiple Object Tracking}{41}
\contentsline {subsection}{\numberline {7.1.3}Testing the Combined Analysis and Neural Network Modules}{42}
\contentsline {subsection}{\numberline {7.1.4}Details of Testing Platform}{42}
\contentsline {subsubsection}{Hardware Platform}{42}
\contentsline {subsubsection}{Software Platform}{42}
\contentsline {section}{\numberline {7.2}{Benchmarking and Testing Face Recognition}}{42}
\contentsline {subsection}{\numberline {7.2.1}Training with LFW}{43}
\contentsline {subsubsection}{Configuration of KSOM}{43}
\contentsline {subsubsection}{Result of training}{43}
\contentsline {subsection}{\numberline {7.2.2}Testing against the LFW}{44}
\contentsline {subsection}{\numberline {7.2.3}Details of Benchmarking Environment}{44}
\contentsline {subsubsection}{Platform Configuration}{44}
\contentsline {subsubsection}{Application Environment}{45}
\contentsline {chapter}{\numberline {8}Conclusion and Future Improvements}{46}
\contentsline {section}{\numberline {8.1}Conclusion}{46}
\contentsline {section}{\numberline {8.2}Future Improvements}{47}
\contentsline {subsection}{\numberline {8.2.1}Improvements to Behaviour Based Surveillance}{47}
\contentsline {subsection}{\numberline {8.2.2}Improvements to Face Recognition}{47}
\contentsline {chapter}{Bibliography}{49}
\contentsline {chapter}{\numberline {A}Models for Face Recognition}{50}
\contentsline {section}{\numberline {A.1}Introduction}{50}
\contentsline {section}{\numberline {A.2}Legacy Models}{50}
\contentsline {section}{\numberline {A.3}Deep Neural Network Models}{51}
\contentsline {chapter}{\numberline {B}Implementation of Kohonen Self Organising Map}{53}
\contentsline {chapter}{\numberline {C}Blob Association and Tracking Algorithm}{59}
\contentsline {section}{\numberline {C.1}Blob Association}{59}
\contentsline {section}{\numberline {C.2}Updating States}{60}
