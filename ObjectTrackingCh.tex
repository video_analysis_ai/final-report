\chapter{Object Tracking}
\section{Introduction}
\hspace{50pt}As the first phase of video surveillance, the various objects moving in the scene are tracked and their motion vectors determined. This involves two major steps: extracting spatial information and updating tracked objects. These are explained below.
\section{Extracting Spatial Information}
\subsection{Background Subtraction}
\hspace{50pt}Background subtraction is the process of separating the pixels corresponding to the foreground of an image from the background. There are many methods available for background subtraction and each one has its own application. For example, some methods are designed for fixed single camera inputs, others for moving camera inputs and still others for multi-camera views of a scene. In this application, the camera input is assumed to be from a typical security camera installation such as a CCTV camera. Hence the camera is fixed and a still background is present. 


\hspace{50pt}The simplest method in such a situation would be to obtain a picture of the background scene alone without any objects in front and then calculate absolute differences between each input frame and the former on a pixel-by-pixel basis. However such a method has two  main drawbacks in practice: firstly, when lighting conditions differ, intensity differences tend to show up even in those locations that correspond to background pixels. Secondly, the problem of shadows come in as pixels corresponding to shadows will invariably get detected as part of the foreground.


\hspace{50pt}To overcome the above difficulties, an adaptive technique has been chosen that can accommodate changes in lighting conditions as well as shadows. It involves constructing a model of the background and then changing it along as the video input is processed. The model of the background is essentially a representation of the pixels that correspond to the background of the scene. It is obtained by maintaining a Gaussian mixture probabilistic model of the pixels which is based on their histories. A pixel showing little variation over time will form part of the background whereas other pixels showing greater variation will be considered part of the foreground. The state of a pixel is decided by looking at its intensity values over a particular number of successive frames called the history. The optimal value of this parameter depends on the degree of zoom and the typical velocities of the moving objects in the scene.


\hspace{50pt}The BackgroundSubtractorMOG2 class in OpenCV has been made use of to implement the background subtraction. The history parameter can be specified when its object is constructed. It has methods that return a gray scale mask of the foreground given an input frame, based on the current background model. It also has algorithms implemented for shadow removal.
\subsection{Noise Filtering}
\hspace{50pt}The output from the background subtractor class contains noises which are due to inaccuracies in the algorithms, general noise in the camera input, etc. This needs to be filtered for better results. The morphological operations of 'open' and 'close' are very useful. The 'open' operation involves erosion of an image (expanding the boundaries of objects) followed by dilation (contracting the boundaries). The 'close' operation involves dilation followed by erosion. The sequence of morphological operations formed by first executing open and then close has a very beneficial effect in removing small noise patches that typically result from background subtraction.
\subsection{Blob Detection and Extraction}
\hspace{50pt}For detecting blobs, the SimpleBlobDetector class of OpenCV has been used which also provides a means of filtering blobs based on areas. This helps to further refine  foreground object detection as most of the 'real' objects will have large enough areas to be filtered.  The SimpleBlobDetector interface provides the centroids of the detected objects.


\hspace{50pt}Using the centroids so obtained, a foreground mask for each individual object is created. This is done by applying the flood fill algorithm on the foreground mask image, starting at each centroid detected. The algorithm finds the pixels reachable from each centroid, thus forming individual masks. These masks are the input for the next phase of updation of tracked objects.
\section{Updating Tracked Objects}
\subsection{States of an Object}
\hspace{50pt}The seminal paper on multi-object tracking \cite{tracking_states} discusses an effective mechanism for keeping track of the various conditions an object goes through while it is tracked and suggests defining a set of states for the objects corresponding to these conditions. This approach has been implemented in this project. The various states of an object are:
\begin{enumerate}
	\item Hypothesis- This is the state that a newly created object assumes. The purpose of this state is to filter out any spurious object detection as any new object is required to get persistently tracked for a certain number of frames while in the hypothesis state before it gets considered as a true object.
	\item Normal state- This is the state of the object after it has been confirmed to be a true object and while it is being tracked frame after frame.
	\item Lost state- This state is assumed by a true object if it disappears (fails to get tracked) in a frame.
	\item Deleted state- This state is assigned to an object when it is sure that the object no longer exists.
	
\end{enumerate}
\subsection{Tracking Algorithm}
\hspace{50pt}The tracking algorithm basically establishes an association between the new blobs extracted in each frame and the objects being tracked. For this, a list of tracked objects is maintained and for each new frame, an attempt is made to find the blob that corresponds to each object. New objects may get added or existing objects deleted in the process. The various steps are:
\begin{enumerate}
	\item Initial association based on proximity- In this step, by comparing the centroid of each new blob with the most recent tracked point of each object, the centroid that is closest to each object is found (only those points that fall within a threshold distance from each object are considered). This step is intended to reduce the number of costly area overlap computations to be performed in the next step.
	\item Finding area of overlap-For each object-blob relation established in step one, the area of overlap between the object's previously tracked blob and the current blob is calculated. The object-blob association is valid only if this value is non-zero.
	\item Creating new objects- New objects are added to the list corresponding to each blob in the current frame that cannot be associated with any existing object.
	\item Updating states of tracked objects- Based on the new object-blob relations obtained, the states of each object are updated. This is based on the transitions between the object states as shown in figure \ref{fig:state_tr} :
	\begin{figure}[h]		
		\includegraphics[width=12cm,height=9cm]{state_tr.jpg}
		\centering
		\caption{Transitions between object states.}
		\label{fig:state_tr}		
	\end{figure}
	\begin{itemize}
		\item Hypothesis-normal transition (1)- This occurs when a newly defined object in the hypothesis state has been persistently tracked for a fixed number of frames. For this, the frame number of the frame in which the object was first detected is stored with the object.
		\item Hypothesis-deleted (2)- When a newly defined object fails to get tracked in a frame while in the hypothesis state, it is regarded as spurious detection and deleted.
		\item Normal-normal transition (3)- When an object in the normal state is again tracked in the current frame, it continues in the normal state.
		\item Normal-Lost transition (4)- When a 'normal' objects fails to get tracked it is marked as 'lost'.
		\item Lost-normal transition (5)- An object can remain 'lost' for some time within the span  of which if it gets detected again, it reenters the normal state.
		\item Lost-deleted transition (6)- An object gets marked as deleted if remains lost for a specified number of frames.
	\end{itemize}
	
	
\end{enumerate}


Using the techniques mentioned above, the motion vectors of each object are obtained. Once an object is deleted, its motion vector is sent to the neural network processing module to determine whether it is to be considered a threat or not.

